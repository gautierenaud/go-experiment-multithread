PKG_LIST=$(go list ./... | grep -v /vendor/)
mkdir -p cover
for package in ${PKG_LIST}; do
    go test -covermode=count -coverprofile "cover/${package##*/}.cov" "$package" ;
done
cat cover/*.cov >> cover/coverage.cov
go tool cover -func=cover/coverage.cov
