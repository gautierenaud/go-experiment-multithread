package thread

import (
	"fmt"
	"math/rand"
	"time"
)

type response struct {
	Content string
	Time    time.Duration
}

// RetrieveOneFromMany will try to retrieve an answer from a query from multiple provider, and will return the first
func RetrieveOneFromMany() {
	rand.Seed(time.Now().UnixNano())

	query := "This is a query"
	// 2 because we need two slots (one per provider)
	response := make(chan response, 2)

	go googleIt(response, query)
	go bingIt(response, query)

	queryResp := <-response

	fmt.Printf("Sent query:\t\t %s\n", query)
	fmt.Printf("Got Response:\t\t %s in %2d\n", queryResp.Content, queryResp.Time)
}

func googleIt(responce chan<- response, query string) {
	waitTime := time.Duration(rand.Intn(10)) * time.Second
	time.Sleep(waitTime)

	responce <- response{"Google", waitTime / time.Second}
}

func bingIt(responce chan<- response, query string) {
	waitTime := time.Duration(rand.Intn(5)) * time.Second
	time.Sleep(waitTime)

	responce <- response{"Bing", waitTime / time.Second}
}
