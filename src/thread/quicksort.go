package thread

import (
	"math/rand"
)

// Quicksort will sort an array of interger into ascending order
func Quicksort(v []int) {
	if len(v) < 1 {
		return
	}

	p := pivot(v)
	low, high := partition(v, p)
	Quicksort(v[:low])
	Quicksort(v[high:])
}

// QuicksortRoutine will sort an array of interger into ascending order with go routines
func QuicksortRoutine(v []int) {
	if len(v) < 1 {
		return
	}

	p := pivot(v)
	low, high := partition(v, p)
	go Quicksort(v[:low])
	go Quicksort(v[high:])
}

func pivot(v []int) int {
	n := len(v)
	return median(v[rand.Intn(n)], v[rand.Intn(n)], v[rand.Intn(n)])
}

func median(a, b, c int) int {
	if a < b {
		switch {
		case b < c:
			return b
		case a < c:
			return c
		default:
			return a
		}
	}
	switch {
	case a < c:
		return a
	case b < c:
		return c
	default:
		return b
	}
}

func partition(v []int, p int) (low, high int) {
	low, high = 0, len(v)

	for mid := 0; mid < high; {
		switch a := v[mid]; {
		case a < p:
			v[mid] = v[low]
			v[low] = a
			low++
			mid++
		case a == p:
			mid++
		default:
			v[mid] = v[high-1]
			v[high-1] = a
			high--
		}
	}

	return
}
