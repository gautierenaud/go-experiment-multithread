package thread

import (
	"fmt"
)

// DoMultiplication will do a multiplication and communicate through a channel 
func DoMultiplication() {
	a := 2
	b := 2

	operationDone := make(chan bool)
	go func (){
		b = a * b

		operationDone <- true
	}()

	// wait for a message to be pushed inside the channel
	<- operationDone

	a = b * b

	fmt.Printf("a = %d, b = %d\n", a, b)
}