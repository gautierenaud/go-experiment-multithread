package thread

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)

// WaitForMany waits for multiple routines to finish before resuming
func WaitForMany() {
	rand.Seed(time.Now().UTC().UnixNano())

	respond := make(chan string, 5)
	var wg sync.WaitGroup

	wg.Add(5)
	go checkDNS(respond, &wg, "address1")
	go checkDNS(respond, &wg, "address2")
	go checkDNS(respond, &wg, "address3")
	go checkDNS(respond, &wg, "address4")
	go checkDNS(respond, &wg, "address5")

	wg.Wait()
	close(respond)

	for queryResp := range respond {
		fmt.Printf("Got Response: \t %s\n", queryResp)
	}
}

func checkDNS(respond chan<- string, wg *sync.WaitGroup, address string) {
	defer wg.Done()

	time.Sleep(time.Duration(rand.Intn(10)) * time.Second)
	respond <- fmt.Sprintf("%s responded", address)
}
