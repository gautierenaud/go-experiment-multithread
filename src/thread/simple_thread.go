package thread

import (
	"fmt"
)

// CallOneThread will just call a goroutine among several print
func CallOneThread() {
	fmt.Println("This will show first")

	go func() {
		fmt.Println("This will happen sometime")
	}()

	fmt.Println("This will be 2nd or 3rd")

	fmt.Scanln()
	fmt.Println("End")
}