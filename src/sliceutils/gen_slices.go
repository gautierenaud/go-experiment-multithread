package sliceutils

import (
	"math/rand"
	"time"
)

// MakeRange will generate a slice of length len with random numbers from 0 to max
func MakeRange(len, max int) []int {
	rand.Seed(time.Now().UTC().UnixNano())

	slice := make([]int, len)

	for i := range slice {
		slice[i] = rand.Intn(max)
	}

	return slice
}
