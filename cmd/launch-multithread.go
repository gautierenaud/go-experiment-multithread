package main

import (
	"fmt"
	"time"

	"gitlab.com/gautierenaud/go-experiment-multithread/src/sliceutils"
	"gitlab.com/gautierenaud/go-experiment-multithread/src/thread"
)

func main() {
	// thread.CallOneThread()
	//thread.DoMultiplication()
	// thread.RetrieveOneFromMany()
	// thread.SelectFastestCase()
	// thread.WaitForMany()

	arrayToSort := sliceutils.MakeRange(100000000, 100)

	arrayToSortLin := make([]int, len(arrayToSort))
	copy(arrayToSortLin, arrayToSort)

	start := time.Now()
	thread.Quicksort(arrayToSortLin)
	end := time.Now()

	fmt.Printf("Linear:\t %d\n", end.Sub(start))

	arrayToSortRout := make([]int, len(arrayToSort))
	copy(arrayToSortRout, arrayToSort)

	start = time.Now()
	thread.QuicksortRoutine(arrayToSortRout)
	end = time.Now()

	fmt.Printf("Multi:\t %d\n", end.Sub(start))
}
